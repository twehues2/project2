import bluetooth

hostMACAddress = "9C:30:5B:D3:7C:DC"
#hostMACAddress = "E4:5F:01:70:F8:3C"
port = 4
backlog = 1
size = 1024
s = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
s.bind((hostMACAddress, port))
s.listen(backlog)
print("listening on port ", port)
try:
    client, clientInfo = s.accept()
    while 1:
        print("server recv from: ", clientInfo)
        data = client.recv(size)
        if data:
            print(data)
            client.send(data)                # Echo back to client
except Exception as e:
    print("Closing socket")
    client.close()
    s.close()
