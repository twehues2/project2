import socket
hostMACAddress = "9C:30:5B:D3:7C:DC"

port = 4
backlog=1
size = 1024

s = socket.socket(socket.AF_BLUETOOTH, socket.SOCK_STREAM, socket.BTPROTO_RFCOMM)
s.bind((hostMACAddress, port))
s.listen(backlog)

try:
  client, address = s.accept()
  while 1:
    data = client.recv(size)
    if data:
      print(data)
      client.send(data)
except Exception as e:
  print("closing socket")
  client.close()
s.close()
