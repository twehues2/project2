var server_port = 65478;
var server_addr = "192.168.0.128";

function client(input){
    
    const net = require('net');
  //  var input = document.getElementById("myName").value;

    const client = net.createConnection({ port: server_port, host: server_addr }, () => {
        // 'connect' listener.
        console.log('connected to server!');
        // send the message
        client.write(`${input}\r\n`);
    });
    
    // get the data from the server
    client.on('data', (data) => {
	console.log(data.toString())
	d2 = data.toString();	
	dList = d2.split(";");
        document.getElementById("speed").innerHTML = dList[1];
	document.getElementById("direction").innerHTML = dList[0];
	document.getElementById("temp").innerHTML = dList[2];
        console.log(data.toString());
        client.end();
        client.destroy();
    });

    client.on('end', () => {
        console.log('disconnected from server');
    });


}

function forward(){

    // get the element from html
    // update the content in html
    // send the data to the server 
    console.log('forward!');
    client("forward");

}
function right(){

    // get the element from html
    // update the content in html
    // send the data to the server 
    console.log('right!');
    client("right");

}

function left(){

    // get the element from html
    // update the content in html
    // send the data to the server 
    console.log('left!');
    client("left");

}

function down(){

    // get the element from html
    // update the content in html
    // send the data to the server 
    console.log('reverse!');
    client("reverse");

}